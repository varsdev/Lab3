﻿namespace ProyectoWS
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.pnlMañanaR = new System.Windows.Forms.Panel();
            this.lblComentarioM = new System.Windows.Forms.TextBox();
            this.lblTituloM = new System.Windows.Forms.Label();
            this.pbxImagenM = new System.Windows.Forms.PictureBox();
            this.pnlTardeR = new System.Windows.Forms.Panel();
            this.lblComentarioT = new System.Windows.Forms.TextBox();
            this.lblTituloT = new System.Windows.Forms.Label();
            this.pbxImagenT = new System.Windows.Forms.PictureBox();
            this.pnlNocheR = new System.Windows.Forms.Panel();
            this.lblComentarioN = new System.Windows.Forms.TextBox();
            this.lblTituloN = new System.Windows.Forms.Label();
            this.pbxImagenN = new System.Windows.Forms.PictureBox();
            this.cbxRegiones = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblComentarioMC = new System.Windows.Forms.TextBox();
            this.lblTituloMC = new System.Windows.Forms.Label();
            this.pbxImagenMC = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblComentarioTC = new System.Windows.Forms.TextBox();
            this.lblTituloTC = new System.Windows.Forms.Label();
            this.pbxImagenTC = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblComentarioNC = new System.Windows.Forms.TextBox();
            this.lblTituloNC = new System.Windows.Forms.Label();
            this.pbxImagenNC = new System.Windows.Forms.PictureBox();
            this.cbxCiudades = new System.Windows.Forms.ComboBox();
            this.lblTempMax = new System.Windows.Forms.TextBox();
            this.lblTempMin = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.pbFaseLunar = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblSolSale = new System.Windows.Forms.Label();
            this.lblLunaSale = new System.Windows.Forms.Label();
            this.lblSolOculta = new System.Windows.Forms.Label();
            this.lblLunaOculta = new System.Windows.Forms.Label();
            this.lblLuna = new System.Windows.Forms.Label();
            this.groupBox4.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.pnlMañanaR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenM)).BeginInit();
            this.pnlTardeR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenT)).BeginInit();
            this.pnlNocheR.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenN)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenMC)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenTC)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenNC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFaseLunar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.flowLayoutPanel1);
            this.groupBox4.Controls.Add(this.cbxRegiones);
            this.groupBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(9, 9);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Size = new System.Drawing.Size(291, 375);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Regional";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.pnlMañanaR);
            this.flowLayoutPanel1.Controls.Add(this.pnlTardeR);
            this.flowLayoutPanel1.Controls.Add(this.pnlNocheR);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(16, 117);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(262, 241);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // pnlMañanaR
            // 
            this.pnlMañanaR.Controls.Add(this.lblComentarioM);
            this.pnlMañanaR.Controls.Add(this.lblTituloM);
            this.pnlMañanaR.Controls.Add(this.pbxImagenM);
            this.pnlMañanaR.Location = new System.Drawing.Point(2, 2);
            this.pnlMañanaR.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlMañanaR.Name = "pnlMañanaR";
            this.pnlMañanaR.Size = new System.Drawing.Size(261, 77);
            this.pnlMañanaR.TabIndex = 0;
            // 
            // lblComentarioM
            // 
            this.lblComentarioM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lblComentarioM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioM.Enabled = false;
            this.lblComentarioM.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioM.ForeColor = System.Drawing.Color.White;
            this.lblComentarioM.Location = new System.Drawing.Point(76, 32);
            this.lblComentarioM.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lblComentarioM.Multiline = true;
            this.lblComentarioM.Name = "lblComentarioM";
            this.lblComentarioM.ReadOnly = true;
            this.lblComentarioM.Size = new System.Drawing.Size(178, 38);
            this.lblComentarioM.TabIndex = 2;
            this.lblComentarioM.Text = "Comentarios";
            // 
            // lblTituloM
            // 
            this.lblTituloM.AutoSize = true;
            this.lblTituloM.Location = new System.Drawing.Point(71, 11);
            this.lblTituloM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTituloM.Name = "lblTituloM";
            this.lblTituloM.Size = new System.Drawing.Size(60, 20);
            this.lblTituloM.TabIndex = 1;
            this.lblTituloM.Text = "Estado";
            // 
            // pbxImagenM
            // 
            this.pbxImagenM.Location = new System.Drawing.Point(2, 3);
            this.pbxImagenM.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbxImagenM.Name = "pbxImagenM";
            this.pbxImagenM.Size = new System.Drawing.Size(66, 69);
            this.pbxImagenM.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenM.TabIndex = 0;
            this.pbxImagenM.TabStop = false;
            // 
            // pnlTardeR
            // 
            this.pnlTardeR.Controls.Add(this.lblComentarioT);
            this.pnlTardeR.Controls.Add(this.lblTituloT);
            this.pnlTardeR.Controls.Add(this.pbxImagenT);
            this.pnlTardeR.Location = new System.Drawing.Point(2, 83);
            this.pnlTardeR.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlTardeR.Name = "pnlTardeR";
            this.pnlTardeR.Size = new System.Drawing.Size(261, 77);
            this.pnlTardeR.TabIndex = 1;
            // 
            // lblComentarioT
            // 
            this.lblComentarioT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lblComentarioT.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioT.Enabled = false;
            this.lblComentarioT.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioT.ForeColor = System.Drawing.Color.White;
            this.lblComentarioT.Location = new System.Drawing.Point(76, 34);
            this.lblComentarioT.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lblComentarioT.Multiline = true;
            this.lblComentarioT.Name = "lblComentarioT";
            this.lblComentarioT.ReadOnly = true;
            this.lblComentarioT.Size = new System.Drawing.Size(178, 38);
            this.lblComentarioT.TabIndex = 2;
            this.lblComentarioT.Text = "Comentarios";
            // 
            // lblTituloT
            // 
            this.lblTituloT.AutoSize = true;
            this.lblTituloT.Location = new System.Drawing.Point(74, 8);
            this.lblTituloT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTituloT.Name = "lblTituloT";
            this.lblTituloT.Size = new System.Drawing.Size(60, 20);
            this.lblTituloT.TabIndex = 1;
            this.lblTituloT.Text = "Estado";
            // 
            // pbxImagenT
            // 
            this.pbxImagenT.Location = new System.Drawing.Point(2, 4);
            this.pbxImagenT.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbxImagenT.Name = "pbxImagenT";
            this.pbxImagenT.Size = new System.Drawing.Size(66, 69);
            this.pbxImagenT.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenT.TabIndex = 0;
            this.pbxImagenT.TabStop = false;
            // 
            // pnlNocheR
            // 
            this.pnlNocheR.Controls.Add(this.lblComentarioN);
            this.pnlNocheR.Controls.Add(this.lblTituloN);
            this.pnlNocheR.Controls.Add(this.pbxImagenN);
            this.pnlNocheR.Location = new System.Drawing.Point(2, 164);
            this.pnlNocheR.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pnlNocheR.Name = "pnlNocheR";
            this.pnlNocheR.Size = new System.Drawing.Size(261, 77);
            this.pnlNocheR.TabIndex = 2;
            // 
            // lblComentarioN
            // 
            this.lblComentarioN.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lblComentarioN.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioN.Enabled = false;
            this.lblComentarioN.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioN.ForeColor = System.Drawing.Color.White;
            this.lblComentarioN.Location = new System.Drawing.Point(78, 32);
            this.lblComentarioN.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.lblComentarioN.Multiline = true;
            this.lblComentarioN.Name = "lblComentarioN";
            this.lblComentarioN.ReadOnly = true;
            this.lblComentarioN.Size = new System.Drawing.Size(174, 38);
            this.lblComentarioN.TabIndex = 2;
            this.lblComentarioN.Text = "Comentarios";
            // 
            // lblTituloN
            // 
            this.lblTituloN.AutoSize = true;
            this.lblTituloN.Location = new System.Drawing.Point(74, 8);
            this.lblTituloN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTituloN.Name = "lblTituloN";
            this.lblTituloN.Size = new System.Drawing.Size(60, 20);
            this.lblTituloN.TabIndex = 1;
            this.lblTituloN.Text = "Estado";
            // 
            // pbxImagenN
            // 
            this.pbxImagenN.Location = new System.Drawing.Point(2, 2);
            this.pbxImagenN.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pbxImagenN.Name = "pbxImagenN";
            this.pbxImagenN.Size = new System.Drawing.Size(66, 69);
            this.pbxImagenN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenN.TabIndex = 0;
            this.pbxImagenN.TabStop = false;
            // 
            // cbxRegiones
            // 
            this.cbxRegiones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.cbxRegiones.DisplayMember = "nombre";
            this.cbxRegiones.ForeColor = System.Drawing.Color.White;
            this.cbxRegiones.FormattingEnabled = true;
            this.cbxRegiones.Location = new System.Drawing.Point(16, 30);
            this.cbxRegiones.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.cbxRegiones.Name = "cbxRegiones";
            this.cbxRegiones.Size = new System.Drawing.Size(264, 28);
            this.cbxRegiones.TabIndex = 0;
            this.cbxRegiones.ValueMember = "idRegion";
            this.cbxRegiones.SelectedIndexChanged += new System.EventHandler(this.boxRegiones_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblTempMin);
            this.groupBox1.Controls.Add(this.lblTempMax);
            this.groupBox1.Controls.Add(this.flowLayoutPanel2);
            this.groupBox1.Controls.Add(this.cbxCiudades);
            this.groupBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(315, 11);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(291, 373);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ciudad";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.panel1);
            this.flowLayoutPanel2.Controls.Add(this.panel2);
            this.flowLayoutPanel2.Controls.Add(this.panel3);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(16, 115);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(262, 241);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblComentarioMC);
            this.panel1.Controls.Add(this.lblTituloMC);
            this.panel1.Controls.Add(this.pbxImagenMC);
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(261, 77);
            this.panel1.TabIndex = 0;
            // 
            // lblComentarioMC
            // 
            this.lblComentarioMC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lblComentarioMC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioMC.Enabled = false;
            this.lblComentarioMC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioMC.ForeColor = System.Drawing.Color.White;
            this.lblComentarioMC.Location = new System.Drawing.Point(76, 32);
            this.lblComentarioMC.Margin = new System.Windows.Forms.Padding(2);
            this.lblComentarioMC.Multiline = true;
            this.lblComentarioMC.Name = "lblComentarioMC";
            this.lblComentarioMC.ReadOnly = true;
            this.lblComentarioMC.Size = new System.Drawing.Size(178, 38);
            this.lblComentarioMC.TabIndex = 2;
            this.lblComentarioMC.Text = "Comentarios";
            // 
            // lblTituloMC
            // 
            this.lblTituloMC.AutoSize = true;
            this.lblTituloMC.Location = new System.Drawing.Point(71, 11);
            this.lblTituloMC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTituloMC.Name = "lblTituloMC";
            this.lblTituloMC.Size = new System.Drawing.Size(60, 20);
            this.lblTituloMC.TabIndex = 1;
            this.lblTituloMC.Text = "Estado";
            // 
            // pbxImagenMC
            // 
            this.pbxImagenMC.Location = new System.Drawing.Point(2, 3);
            this.pbxImagenMC.Margin = new System.Windows.Forms.Padding(2);
            this.pbxImagenMC.Name = "pbxImagenMC";
            this.pbxImagenMC.Size = new System.Drawing.Size(66, 69);
            this.pbxImagenMC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenMC.TabIndex = 0;
            this.pbxImagenMC.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblComentarioTC);
            this.panel2.Controls.Add(this.lblTituloTC);
            this.panel2.Controls.Add(this.pbxImagenTC);
            this.panel2.Location = new System.Drawing.Point(2, 83);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(261, 77);
            this.panel2.TabIndex = 1;
            // 
            // lblComentarioTC
            // 
            this.lblComentarioTC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lblComentarioTC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioTC.Enabled = false;
            this.lblComentarioTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioTC.ForeColor = System.Drawing.Color.White;
            this.lblComentarioTC.Location = new System.Drawing.Point(76, 34);
            this.lblComentarioTC.Margin = new System.Windows.Forms.Padding(2);
            this.lblComentarioTC.Multiline = true;
            this.lblComentarioTC.Name = "lblComentarioTC";
            this.lblComentarioTC.ReadOnly = true;
            this.lblComentarioTC.Size = new System.Drawing.Size(178, 38);
            this.lblComentarioTC.TabIndex = 2;
            this.lblComentarioTC.Text = "Comentarios";
            // 
            // lblTituloTC
            // 
            this.lblTituloTC.AutoSize = true;
            this.lblTituloTC.Location = new System.Drawing.Point(74, 8);
            this.lblTituloTC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTituloTC.Name = "lblTituloTC";
            this.lblTituloTC.Size = new System.Drawing.Size(60, 20);
            this.lblTituloTC.TabIndex = 1;
            this.lblTituloTC.Text = "Estado";
            // 
            // pbxImagenTC
            // 
            this.pbxImagenTC.Location = new System.Drawing.Point(2, 4);
            this.pbxImagenTC.Margin = new System.Windows.Forms.Padding(2);
            this.pbxImagenTC.Name = "pbxImagenTC";
            this.pbxImagenTC.Size = new System.Drawing.Size(66, 69);
            this.pbxImagenTC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenTC.TabIndex = 0;
            this.pbxImagenTC.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblComentarioNC);
            this.panel3.Controls.Add(this.lblTituloNC);
            this.panel3.Controls.Add(this.pbxImagenNC);
            this.panel3.Location = new System.Drawing.Point(2, 164);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(261, 77);
            this.panel3.TabIndex = 2;
            // 
            // lblComentarioNC
            // 
            this.lblComentarioNC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lblComentarioNC.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblComentarioNC.Enabled = false;
            this.lblComentarioNC.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComentarioNC.ForeColor = System.Drawing.Color.White;
            this.lblComentarioNC.Location = new System.Drawing.Point(78, 32);
            this.lblComentarioNC.Margin = new System.Windows.Forms.Padding(2);
            this.lblComentarioNC.Multiline = true;
            this.lblComentarioNC.Name = "lblComentarioNC";
            this.lblComentarioNC.ReadOnly = true;
            this.lblComentarioNC.Size = new System.Drawing.Size(174, 38);
            this.lblComentarioNC.TabIndex = 2;
            this.lblComentarioNC.Text = "Comentarios";
            // 
            // lblTituloNC
            // 
            this.lblTituloNC.AutoSize = true;
            this.lblTituloNC.Location = new System.Drawing.Point(74, 8);
            this.lblTituloNC.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTituloNC.Name = "lblTituloNC";
            this.lblTituloNC.Size = new System.Drawing.Size(60, 20);
            this.lblTituloNC.TabIndex = 1;
            this.lblTituloNC.Text = "Estado";
            // 
            // pbxImagenNC
            // 
            this.pbxImagenNC.Location = new System.Drawing.Point(2, 2);
            this.pbxImagenNC.Margin = new System.Windows.Forms.Padding(2);
            this.pbxImagenNC.Name = "pbxImagenNC";
            this.pbxImagenNC.Size = new System.Drawing.Size(66, 69);
            this.pbxImagenNC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxImagenNC.TabIndex = 0;
            this.pbxImagenNC.TabStop = false;
            // 
            // cbxCiudades
            // 
            this.cbxCiudades.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.cbxCiudades.DisplayMember = "nombre";
            this.cbxCiudades.ForeColor = System.Drawing.Color.White;
            this.cbxCiudades.FormattingEnabled = true;
            this.cbxCiudades.Location = new System.Drawing.Point(16, 30);
            this.cbxCiudades.Margin = new System.Windows.Forms.Padding(2);
            this.cbxCiudades.Name = "cbxCiudades";
            this.cbxCiudades.Size = new System.Drawing.Size(264, 28);
            this.cbxCiudades.TabIndex = 0;
            this.cbxCiudades.ValueMember = "idRegion";
            this.cbxCiudades.SelectedIndexChanged += new System.EventHandler(this.cbxCiudades_SelectedIndexChanged);
            // 
            // lblTempMax
            // 
            this.lblTempMax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lblTempMax.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblTempMax.Enabled = false;
            this.lblTempMax.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempMax.ForeColor = System.Drawing.Color.White;
            this.lblTempMax.Location = new System.Drawing.Point(16, 63);
            this.lblTempMax.Margin = new System.Windows.Forms.Padding(2);
            this.lblTempMax.Multiline = true;
            this.lblTempMax.Name = "lblTempMax";
            this.lblTempMax.ReadOnly = true;
            this.lblTempMax.Size = new System.Drawing.Size(178, 21);
            this.lblTempMax.TabIndex = 3;
            this.lblTempMax.Text = "Comentarios";
            // 
            // lblTempMin
            // 
            this.lblTempMin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.lblTempMin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblTempMin.Enabled = false;
            this.lblTempMin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTempMin.ForeColor = System.Drawing.Color.White;
            this.lblTempMin.Location = new System.Drawing.Point(16, 78);
            this.lblTempMin.Margin = new System.Windows.Forms.Padding(2);
            this.lblTempMin.Multiline = true;
            this.lblTempMin.Name = "lblTempMin";
            this.lblTempMin.ReadOnly = true;
            this.lblTempMin.Size = new System.Drawing.Size(178, 21);
            this.lblTempMin.TabIndex = 4;
            this.lblTempMin.Text = "Comentarios";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(668, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Efemerides";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.ForeColor = System.Drawing.Color.White;
            this.lblFecha.Location = new System.Drawing.Point(635, 74);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(34, 13);
            this.lblFecha.TabIndex = 5;
            this.lblFecha.Text = "fecha";
            // 
            // pbFaseLunar
            // 
            this.pbFaseLunar.AccessibleDescription = "asdasd";
            this.pbFaseLunar.Location = new System.Drawing.Point(619, 145);
            this.pbFaseLunar.Margin = new System.Windows.Forms.Padding(2);
            this.pbFaseLunar.Name = "pbFaseLunar";
            this.pbFaseLunar.Size = new System.Drawing.Size(50, 55);
            this.pbFaseLunar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbFaseLunar.TabIndex = 8;
            this.pbFaseLunar.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.AccessibleDescription = "asdasd";
            this.pictureBox1.ImageLocation = "https://www.imn.ac.cr/Portal-IMN-Principal-theme/images/efemerides/efemerides-sol" +
    "-sale%20y%20pone-blanco.png?pfdrid_c=true";
            this.pictureBox1.Location = new System.Drawing.Point(615, 222);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(58, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.AccessibleDescription = "asdasd";
            this.pictureBox2.ImageLocation = "https://www.imn.ac.cr/Portal-IMN-Principal-theme/images/efemerides/efemerides-lun" +
    "a-sale%20y%20pone-blanco.png?pfdrid_c=true";
            this.pictureBox2.Location = new System.Drawing.Point(618, 298);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(51, 63);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(686, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Fase Lunar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(686, 224);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Efemeride Sol";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(686, 303);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Efemeride Luna";
            // 
            // lblSolSale
            // 
            this.lblSolSale.AutoSize = true;
            this.lblSolSale.ForeColor = System.Drawing.Color.White;
            this.lblSolSale.Location = new System.Drawing.Point(686, 243);
            this.lblSolSale.Name = "lblSolSale";
            this.lblSolSale.Size = new System.Drawing.Size(31, 13);
            this.lblSolSale.TabIndex = 14;
            this.lblSolSale.Text = "Sale:";
            // 
            // lblLunaSale
            // 
            this.lblLunaSale.AutoSize = true;
            this.lblLunaSale.ForeColor = System.Drawing.Color.White;
            this.lblLunaSale.Location = new System.Drawing.Point(686, 322);
            this.lblLunaSale.Name = "lblLunaSale";
            this.lblLunaSale.Size = new System.Drawing.Size(31, 13);
            this.lblLunaSale.TabIndex = 15;
            this.lblLunaSale.Text = "Sale:";
            // 
            // lblSolOculta
            // 
            this.lblSolOculta.AutoSize = true;
            this.lblSolOculta.ForeColor = System.Drawing.Color.White;
            this.lblSolOculta.Location = new System.Drawing.Point(686, 264);
            this.lblSolOculta.Name = "lblSolOculta";
            this.lblSolOculta.Size = new System.Drawing.Size(50, 13);
            this.lblSolOculta.TabIndex = 16;
            this.lblSolOculta.Text = "Se pone:";
            // 
            // lblLunaOculta
            // 
            this.lblLunaOculta.AutoSize = true;
            this.lblLunaOculta.ForeColor = System.Drawing.Color.White;
            this.lblLunaOculta.Location = new System.Drawing.Point(686, 347);
            this.lblLunaOculta.Name = "lblLunaOculta";
            this.lblLunaOculta.Size = new System.Drawing.Size(50, 13);
            this.lblLunaOculta.TabIndex = 17;
            this.lblLunaOculta.Text = "Se pone:";
            // 
            // lblLuna
            // 
            this.lblLuna.AutoSize = true;
            this.lblLuna.ForeColor = System.Drawing.Color.White;
            this.lblLuna.Location = new System.Drawing.Point(686, 169);
            this.lblLuna.Name = "lblLuna";
            this.lblLuna.Size = new System.Drawing.Size(30, 13);
            this.lblLuna.TabIndex = 18;
            this.lblLuna.Text = "Fase";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.ClientSize = new System.Drawing.Size(800, 395);
            this.Controls.Add(this.lblLuna);
            this.Controls.Add(this.lblLunaOculta);
            this.Controls.Add(this.lblSolOculta);
            this.Controls.Add(this.lblLunaSale);
            this.Controls.Add(this.lblSolSale);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pbFaseLunar);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox4);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Opacity = 0.97D;
            this.Text = "Pronóstico del Tiempo";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox4.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.pnlMañanaR.ResumeLayout(false);
            this.pnlMañanaR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenM)).EndInit();
            this.pnlTardeR.ResumeLayout(false);
            this.pnlTardeR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenT)).EndInit();
            this.pnlNocheR.ResumeLayout(false);
            this.pnlNocheR.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenN)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenMC)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenTC)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImagenNC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbFaseLunar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel pnlMañanaR;
        private System.Windows.Forms.TextBox lblComentarioM;
        private System.Windows.Forms.Label lblTituloM;
        private System.Windows.Forms.PictureBox pbxImagenM;
        private System.Windows.Forms.Panel pnlTardeR;
        private System.Windows.Forms.TextBox lblComentarioT;
        private System.Windows.Forms.Label lblTituloT;
        private System.Windows.Forms.PictureBox pbxImagenT;
        private System.Windows.Forms.Panel pnlNocheR;
        private System.Windows.Forms.TextBox lblComentarioN;
        private System.Windows.Forms.Label lblTituloN;
        private System.Windows.Forms.PictureBox pbxImagenN;
        private System.Windows.Forms.ComboBox cbxRegiones;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox lblComentarioMC;
        private System.Windows.Forms.Label lblTituloMC;
        private System.Windows.Forms.PictureBox pbxImagenMC;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox lblComentarioTC;
        private System.Windows.Forms.Label lblTituloTC;
        private System.Windows.Forms.PictureBox pbxImagenTC;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox lblComentarioNC;
        private System.Windows.Forms.Label lblTituloNC;
        private System.Windows.Forms.PictureBox pbxImagenNC;
        private System.Windows.Forms.ComboBox cbxCiudades;
        private System.Windows.Forms.TextBox lblTempMax;
        private System.Windows.Forms.TextBox lblTempMin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.PictureBox pbFaseLunar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblSolSale;
        private System.Windows.Forms.Label lblLunaSale;
        private System.Windows.Forms.Label lblSolOculta;
        private System.Windows.Forms.Label lblLunaOculta;
        private System.Windows.Forms.Label lblLuna;
    }
}


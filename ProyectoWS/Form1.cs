﻿using System;
using ProyectoWS.IMN_WS;
using System.Windows.Forms;
using DemoWebService;
using System.Linq;

namespace ProyectoWS
{
    public partial class Form1 : Form
    {
        private string RUTA_LUNA = "https://www.imn.ac.cr/Portal-IMN-Principal-theme/images/efemerides/Luna-{0}-blanco.png";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CargarRegiones();
            CargarEfemerides();

        }
        private void CargarRegiones()
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient();
            PRONOSTICO_REGIONAL reg = ws.pronosticoRegional(new pronosticoRegion()).ParseXML<PRONOSTICO_REGIONAL>();
            cbxRegiones.DataSource = reg.REGIONES;
        }

        private void CargarEfemerides()
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient();
            EFEMERIDES efeme = ws.efemerides(new efemerides()).ParseXML<EFEMERIDES>();
            string fecha = ws.fecha("").ToClean();
            fecha = fecha.Replace(fecha.ElementAt(0), Char.ToUpper(fecha.ElementAt(0)));
            lblFecha.Text = fecha;
            pbFaseLunar.ImageLocation = string.Format(RUTA_LUNA, efeme.FASELUNAR.Value.Replace(" ", "-").ToLower());
            lblLuna.Text = efeme.FASELUNAR.Value;
            lblSolSale.Text = lblSolSale.Text + " " + efeme.EFEMERIDE_SOL.SALE;
            lblSolOculta.Text = lblSolOculta.Text + " " + efeme.EFEMERIDE_SOL.SEPONE;
            lblLunaSale.Text = lblLunaSale.Text + " " + efeme.EFEMERIDE_LUNA.SALE;
            lblLunaOculta.Text = lblLunaOculta.Text + " " + efeme.EFEMERIDE_LUNA.SEPONE;
        }

        private void boxRegiones_SelectedIndexChanged(object sender, EventArgs e)
        {
            WSMeteorologicoClient ws = new WSMeteorologicoClient();
            if (cbxRegiones.SelectedIndex >= 0)
            {
                int id = (cbxRegiones.SelectedItem as PRONOSTICO_REGIONALREGION)?.idRegion ?? -1;         
                if (id != -1)
                {
                    PRONOSTICO_REGIONAL region = ws.pronosticoRegionalxID(id).ParseXML<PRONOSTICO_REGIONAL>();
                    cbxCiudades.DataSource = region.REGIONES[0].CIUDADES;   
                    if (region.REGIONES[0].ESTADOMANANA != null)
                    {
                        pbxImagenM.ImageLocation = String.Format("https://www.imn.ac.cr{0}", region.REGIONES[0].ESTADOMANANA.imgPath);
                        lblComentarioM.Text = region.REGIONES[0].COMENTARIOMANANA;
                        lblTituloM.Text = region.REGIONES[0].ESTADOMANANA.Value;
                        pnlMañanaR.Show();
                    }

                    if (region.REGIONES[0].ESTADOTARDE != null)
                    {
                        pbxImagenT.ImageLocation = String.Format("https://www.imn.ac.cr{0}", region.REGIONES[0].ESTADOTARDE.imgPath);
                        lblComentarioT.Text = region.REGIONES[0].COMENTARIOTARDE;
                        lblTituloT.Text = region.REGIONES[0].ESTADOTARDE.Value;
                        pnlTardeR.Show();
                    }
                    if (region.REGIONES[0].ESTADONOCHE != null)
                    {
                        pbxImagenN.ImageLocation = String.Format("https://www.imn.ac.cr{0}", region.REGIONES[0].ESTADONOCHE.imgPath);
                        lblComentarioN.Text = region.REGIONES[0].COMENTARIONOCHE;
                        lblTituloN.Text = region.REGIONES[0].ESTADONOCHE.Value;
                        pnlNocheR.Show();
                    }
                }
            }
        }

        private void cbxCiudades_SelectedIndexChanged(object sender, EventArgs e)
        {
            PRONOSTICO_REGIONALREGIONCIUDAD ciudad = cbxCiudades.SelectedItem as PRONOSTICO_REGIONALREGIONCIUDAD;
            lblTempMax.Clear();
            lblTempMin.Clear();
            lblTempMax.AppendText("\nTemperatura Maxima: " + ciudad.TEMPMAX);
            lblTempMin.AppendText("\nTemperatura Minima: " + ciudad.TEMPMIN);

            if (cbxCiudades.SelectedIndex >= 0)
            {
                int id = (cbxCiudades.SelectedItem as PRONOSTICO_REGIONALREGIONCIUDAD)?.id ?? -1;
                if (id != -1)
                {

                    if (ciudad.ESTADOMANANA != null)
                    {
                        pbxImagenMC.ImageLocation = String.Format("https://www.imn.ac.cr{0}", ciudad.ESTADOMANANA.imgPath);
                        lblComentarioMC.Text = ciudad.COMENTARIOMANANA;
                        lblTituloMC.Text = ciudad.ESTADOMANANA.Value;
                        pnlMañanaR.Show();
                    }

                    if (ciudad.ESTADOTARDE != null)
                    {
                        pbxImagenTC.ImageLocation = String.Format("https://www.imn.ac.cr{0}", ciudad.ESTADOTARDE.imgPath);
                        lblComentarioTC.Text = ciudad.COMENTARIOTARDE;
                        lblTituloTC.Text = ciudad.ESTADOTARDE.Value;
                        pnlTardeR.Show();
                    }
                    if (ciudad.ESTADONOCHE != null)
                    {
                        pbxImagenNC.ImageLocation = String.Format("https://www.imn.ac.cr{0}", ciudad.ESTADONOCHE.imgPath);
                        lblComentarioNC.Text = ciudad.COMENTARIONOCHE;
                        lblTituloNC.Text = ciudad.ESTADONOCHE.Value;
                        pnlNocheR.Show();
                    }
                }
            }
        }

    }
}
